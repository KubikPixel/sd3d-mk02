import {
  AmbientLight,
  PointLight,
  Mesh,
  MathUtils,
  Vector3,
  MeshPhongMaterial
} from 'three'
import { SceneLoader } from './sceneloader'
import { Sequences } from './sequences'
import ASScroll from '@ashthornton/asscroll'

export class SetStage extends SceneLoader {
  private moon: Mesh
  private grid: Mesh
  private plane: Mesh
  private ship: Mesh
  protected scrollAni: ASScroll

  constructor(canvas: string, bgimg?: string, scrollAni?: ASScroll) {
    super(canvas, bgimg, scrollAni)
    this.scrollAni = scrollAni

    // stars
    const amountOfStars: number = 1000
    for(let iter: number = 0; iter < amountOfStars; iter++) {
      this.addToStage(this.setStar())
    }
    // moon
    this.moon = this.setPlanet()
    this.moon.position.set(0, 12, -40)
    this.addToStage(this.moon)
    // grid
    this.grid = this.setGrid()
    this.grid.rotateX(MathUtils.degToRad(270))
    this.addToStage(this.grid)
    // plane
    this.plane = this.setPlane()
    this.plane.rotateX(MathUtils.degToRad(270))
    this.plane.position.setY(-0.1)
    this.addToStage(this.plane)
    // ship
    this.ship = this.setShip()
    this.addToStage(this.ship)
    this.ship.rotateZ(MathUtils.degToRad(90))
    this.ship.rotateX(MathUtils.degToRad(90))

    // camera
    this.camera.rotateX(MathUtils.degToRad(10))

    // lights
    const pointlight = new PointLight(0xDDDDFD, 0.7)
    pointlight.position.set(-5, 1, -30)
    this.addToStage(pointlight)
    this.addToStage(new AmbientLight(0xDDDDFF, 0.75))
  }

  private scrollAnimate(): any {
    // const scrollDist: DOMRect = document.body.getBoundingClientRect()
    // const scrollTopPos: number = scrollDist.top

    // // change movement system
    // if (scrollTopPos > -6800) {
    //     this.camera.position.setY(scrollTopPos * 0.005 + 35)
    //   this.camera.position.setZ(20)
    // }

    // if (scrollTopPos > -6000) { // && scrollTopPos > toggleScroll[1]) {
    //   this.camera.position.setZ((scrollTopPos + -scrollTopPos) * 0.001 + 20)
    // }

    // const disScale: number  = (scrollTopPos + 6060) * 0.001
    // const disBias: number = (scrollTopPos + 6060) * -0.001
    // const disPlane: MeshPhongMaterial = this.plane.material as MeshPhongMaterial
    // const disGrid: MeshPhongMaterial = this.grid.material as MeshPhongMaterial
    // disPlane.displacementScale  = disScale
    // disPlane.displacementBias  = disBias
    // this.plane.geometry.computeVertexNormals()
    // disGrid.displacementScale  = disScale
    // disGrid.displacementBias  = disBias

    // this.ship.position.set(0, 0.1, scrollTopPos * 0.0011 + 20)
    // const lookAt: Vector3 = this.ship.position
    // // this.camera.lookAt(lookAt.x, lookAt.y, 0)
  }

  private mouseAnimate(): any {}

  /** Permanent animations
  * @private animate Anitmate continius movement to the whole livetime
  */
  private animate(): void {
    this.moon.rotateY(0.006)
  }

  public playStage = (): void => {
    window.requestAnimationFrame(this.playStage)
    document.body.onscroll = this.scrollAnimate()
    document.body.onmousemove = this.mouseAnimate()
    this.animate()
    this.renderer.render(this.scene, this.camera)
  }
}
