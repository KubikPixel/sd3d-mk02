import { Object3D, Matrix4 } from "three"

export class Sequences {
  private scrollTopPos: number
  private scrollYPos: number
  private screenHeight: number
  private mousePos: MouseEvent

  constructor() {
    const scrollDist: DOMRect = document.body.getBoundingClientRect()
    this.scrollTopPos = scrollDist.top
    this.scrollYPos = scrollDist.y
    this.screenHeight = window.innerHeight
  }

  public startSeq(triggerID: string): boolean {
    const inScreen: number = document.getElementById(triggerID).getBoundingClientRect().y
    return (inScreen < this.screenHeight && inScreen > 0)
  }

  public objPos(obj3d: Object3D, transform: Matrix4): void {
    obj3d.matrixAutoUpdate = true
    obj3d.applyMatrix4(transform)
  }
}
