enum LoadObjectType {
  Mesh = 0,
  Texture = 1,
  CubeTexture = 2
}

import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  Mesh,
  SphereGeometry,
  PlaneGeometry,
  CapsuleGeometry,
  MeshPhongMaterial,
  FrontSide,
  LoadingManager,
  TextureLoader,
  CubeTextureLoader
} from 'three'
import ASScroll from '@ashthornton/asscroll'
import tex_planet from '../assets/img/lroc_color_poles_1k.jpg'
import bmp_planet from '../assets/img/ldem_3_8bit.jpg'
import bmp_plane from '../assets/img/street.png'

export class SceneLoader {
  private static loadMngr: LoadingManager
  protected static maxLoad: number = 6
  protected static numLoad: number = 0

  private canvas: HTMLElement
  protected scrollAni: ASScroll
  protected scene: Scene
  protected camera: PerspectiveCamera
  protected renderer: WebGLRenderer

  constructor(canvas: string, bgimg?: string, scrollAni?: ASScroll) {
    // Loading Splash Screen
    SceneLoader.loadMngr = new LoadingManager()
    const loadingBar:HTMLElement = document.getElementById('loadingprogress')
    SceneLoader.loadMngr.onProgress = (url, loaded, total) => {
      const progress: string = '' + String(100 / total * loaded)
      loadingBar.innerHTML = progress + '% | ' + url
      loadingBar.setAttribute('value', progress)
    }
    const loadingSplash:HTMLElement = document.getElementById('loading')
    SceneLoader.loadMngr.onLoad = () => {
      loadingSplash.remove()
      const docBody: HTMLBodyElement = document.getElementsByTagName('body')[0]
      const titleSplash:HTMLElement = document.getElementById('title')
      docBody.setAttribute('style', 'overflow: scroll')
      titleSplash.setAttribute('style', 'filter: none')
    }

    if (scrollAni) {
      this.scrollAni = scrollAni
    }

    this.scene = new Scene()
    this.scene.background = SceneLoader.loadEntity(bgimg, LoadObjectType.Texture)

    this.canvas = document.getElementById(canvas)
    this.renderer = new WebGLRenderer({ canvas: this.canvas })

    this.setStage()
    window.addEventListener('resize', () => {
      this.setStage()
    })
  }

  private setStage() {
    const wWidth: number = window.innerWidth
    const wHeight: number = window.innerHeight

    this.camera = new PerspectiveCamera(
      60,
      wHeight / wWidth,
      0.1,
      100
    )
    this.camera.updateProjectionMatrix()
    this.renderer.setSize(
      wWidth,
      wHeight
    )
    this.renderer.setPixelRatio(window.devicePixelRatio)
    this.scrollAni.resize({
      width: wWidth,
      height: wHeight
    })
  }

  public getStage(): Scene {
    return this.scene
  }

  public addToStage(obj: any): void {
    if (obj.isObject3D) {
      this.scene.add(obj)
    }
  }

  public static loadEntity(url: string, objtype: LoadObjectType): any {
    if (typeof url == 'string') {
      SceneLoader.loadMngr.onStart = (url, item, total) => {
        // implement loading logic
        console.debug('Loading: ' + url + ' ' + item + '/' + total)
      }
      switch (objtype) {
        case LoadObjectType.Texture:
          return new TextureLoader(SceneLoader.loadMngr).load(url)
        case LoadObjectType.CubeTexture:
          return new CubeTextureLoader(SceneLoader.loadMngr).load(
            this.cubeTexURLs(url)
        )
        case LoadObjectType.Mesh:
          return undefined
      }
    }
    else {
      console.error( "Entity loading path is not a string" )
      return undefined
    }
  }

  private static cubeTexURLs(url: string): Array<string> {
    return [
      url + '-c00.jpeg',
      url + '-c01.jpeg',
      url + '-c02.jpeg',
      url + '-c03.jpeg',
      url + '-c04.jpeg',
      url + '-c05.jpeg'
    ]
  }

  public playStage = (): void => {}

  public setStar(
    spread: number = 100,
    color: number = 0xAAAAFF,
    emission: number = 2
  ): Mesh {
    const star_geo = new SphereGeometry(0.08, 5, 5)
    const star_mat = new MeshPhongMaterial({
      color: color,
      emissive: color,
      emissiveIntensity: emission
    })
    const star = new Mesh(star_geo, star_mat)
    star.position.set(
      (Math.random() - 0.5) * spread,
      (Math.random() - 0.5) * spread,
      (Math.random() - 0.5) * spread
    )
    return star
  }

  public setPlanet(
    texmap: string = tex_planet,
    bmpmap: string = bmp_planet,
    dia: number = 10,
    seg: number = 20
  ): Mesh {
    const planet_geo = new SphereGeometry(dia, seg, seg)
    const planet_tex = SceneLoader.loadEntity(texmap, LoadObjectType.Texture)
    const planet_bmp = SceneLoader.loadEntity(bmpmap, LoadObjectType.Texture)
    const planet_mat = new MeshPhongMaterial({
      map: planet_tex,
      bumpMap: planet_bmp,
      emissive: 0xFCDCDCD,
      emissiveMap: planet_tex,
      emissiveIntensity: 0.1
    })
    return new Mesh(planet_geo, planet_mat)
  }

  public setPlane(
    bmpmap: string = bmp_plane,
    width: number = 20,
    height: number = 40
  ): Mesh {
    const plane_geo = new PlaneGeometry(width, height, width * 2, height * 2)
    const plane_bmp = SceneLoader.loadEntity(bmpmap, LoadObjectType.Texture)
    const plane_mat = new MeshPhongMaterial({
      color: 0x222244,
      displacementMap: plane_bmp,
      displacementScale: 0,
      displacementBias: 0,
      shadowSide: FrontSide,
      shininess: 280
    })
    return new Mesh(plane_geo, plane_mat)
  }

  public setGrid(
    bmpmap: string = bmp_plane,
    width: number = 20,
    height: number = 40
  ): Mesh {
    const plane_geo = new PlaneGeometry(width, height, width * 2, height * 2)
    const plane_bmp = SceneLoader.loadEntity(bmpmap, LoadObjectType.Texture)
    const plane_mat = new MeshPhongMaterial({
      color: 0xCCCCFF,
      wireframe: true,
      displacementMap: plane_bmp,
      displacementScale: 0,
      displacementBias: 0,
      emissive: 0x0000FF,
      emissiveIntensity: 2
    })
    return new Mesh(plane_geo, plane_mat)
  }

  public setShip(
    model: string = '',
    texmap: string = '',
    bmpmap: string = '',
    scale: number = 1
  ): Mesh {
    // placeholder capsule
    const ship_geo = new CapsuleGeometry(0.1, 0.2, 10)
    const ship_tex = SceneLoader.loadEntity(texmap, LoadObjectType.Texture)
    const ship_pmb = SceneLoader.loadEntity(bmpmap, LoadObjectType.Texture)
    const shiip_mat = new MeshPhongMaterial({
      color: 0x00FF00,
      shadowSide: FrontSide
    })
    return new Mesh(ship_geo, shiip_mat)
  }
}
