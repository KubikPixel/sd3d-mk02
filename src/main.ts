import { SetStage } from './stage/setstage'
import WebGL from 'three/examples/jsm/capabilities/WebGL'
import ASScroll from '@ashthornton/asscroll'
import bgimg from './assets/img/nightsky.jpg'

/**
* Entry point checks is WebGL available and set the background image.
* @author KubikPixel
* @remarks Run the entry point for the WebGL Scroll Demo
* @global main - Set class SetStage to play the sett on scene
* @param canvas - ID of the HTML Canvas to draw
* @param bkimg - Background image for the WebGL render canvas
*/
const main = (canvas: string, bkimg: string): void => {
  const asscroll = new ASScroll({
    disableResize: true
  })
  window.addEventListener('load', () => {
    asscroll.enable()
  })

  if (WebGL.isWebGLAvailable()) {
    const scene = new SetStage(canvas, bkimg, asscroll);
    scene.playStage()
  }
  else {
    const warning = WebGL.getWebGLErrorMessage()
    document.getElementById('msg').appendChild(warning)
  }
}

main('draw',bgimg)
