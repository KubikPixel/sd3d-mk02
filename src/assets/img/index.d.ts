declare module '*.jpg' {
  const jpg: any
  export = jpg
}

declare module '*.png' {
  const png: any
  export = png
}
