import { SceneLoader } from './sceneloader';
import ASScroll from '@ashthornton/asscroll';
export declare class SetStage extends SceneLoader {
    private moon;
    private grid;
    private plane;
    private ship;
    protected scrollAni: ASScroll;
    constructor(canvas: string, bgimg?: string, scrollAni?: ASScroll);
    private scrollAnimate;
    private mouseAnimate;
    private animate;
    playStage: () => void;
}
//# sourceMappingURL=setstage.d.ts.map