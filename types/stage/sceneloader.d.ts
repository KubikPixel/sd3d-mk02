declare enum LoadObjectType {
    Mesh = 0,
    Texture = 1,
    CubeTexture = 2
}
import { Scene, PerspectiveCamera, WebGLRenderer, Mesh } from 'three';
import ASScroll from '@ashthornton/asscroll';
export declare class SceneLoader {
    private static loadMngr;
    protected static maxLoad: number;
    protected static numLoad: number;
    private canvas;
    protected scrollAni: ASScroll;
    protected scene: Scene;
    protected camera: PerspectiveCamera;
    protected renderer: WebGLRenderer;
    constructor(canvas: string, bgimg?: string, scrollAni?: ASScroll);
    private setStage;
    getStage(): Scene;
    addToStage(obj: any): void;
    static loadEntity(url: string, objtype: LoadObjectType): any;
    private static cubeTexURLs;
    playStage: () => void;
    setStar(spread?: number, color?: number, emission?: number): Mesh;
    setPlanet(texmap?: string, bmpmap?: string, dia?: number, seg?: number): Mesh;
    setPlane(bmpmap?: string, width?: number, height?: number): Mesh;
    setGrid(bmpmap?: string, width?: number, height?: number): Mesh;
    setShip(model?: string, texmap?: string, bmpmap?: string, scale?: number): Mesh;
}
export {};
//# sourceMappingURL=sceneloader.d.ts.map