import { Object3D, Matrix4 } from "three";
export declare class Sequences {
    private scrollTopPos;
    private scrollYPos;
    private screenHeight;
    private mousePos;
    constructor();
    startSeq(triggerID: string): boolean;
    objPos(obj3d: Object3D, transform: Matrix4): void;
}
//# sourceMappingURL=sequences.d.ts.map