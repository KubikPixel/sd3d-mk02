# SD.3D-MK02

[You can check it out here...](https://thunix.net/~kubikpixel/webgl-scroll-demo/index.html)

## WebGL Scroll Demo

A simple scrolling demo for browser compatibility tests for my build tool and WebGL.

This is my personal little playground to figure out how ThreeJS with TypeScript works fine together.

## Documentation and build

To build this project you need `nodejs` and `yarn`.

```sh
# clone repo and install with yarn
git clone https://codeberg.org/KubikPixel/sd3d-mk02.git
cd sd3d-mk02
yarn install
# build project in the ./dist folder
yarn build
# create documentation
yarn docs
```
